# Introduction

The goal of this project is to create an IRC Bot to help organising lunch events with colleagues.


# Acceptance Criteria

This is the list of acceptance criteria for this project.  
This project intends to use [subplot] to document and test the acceptance criteria

## It is possible to create a plan

```scenario
given a running bot
when I create a new plan "new plan"
then I get a creation success response
when I get the list of existing plans
then I get a list of plans
and the list contains "new plan"
```

## It is possible to delete an existing plan

```scenario
given a runnig bot
and an existing plan "del plan"
when I delete the existing plan "del plan"
then I get a deletion success response
when I get the list of existing plans
then I get a list of plans
and the list does not contains plan "del plan"
```

## A user can see existing plans

```scenario
given a runnig bot
and an existing plan "first plan"
and an existing plan "second plan"
when I list the existing plans
then I get a list of plans
and the list contains plan "first plan"
and the list contains plan "second plan"
```

## A user can join a plan

```scenario
given a runnig bot
and an existing plan "join plan"
when I join plan "join plan"
then I get a join success response
when I get plan "join plan"
then I appear in the list of attendees
```

## A user can leave a plan

```scenario
given a runnig bot
and an existing plan "leave plan"
and I am part of "leave plan" attendees
when I leave plan "leave plan"
then I get a leave success response
when I get plan "leave plan"
then I do not appear in the list of attendees
```


[subplot]: https://gitlab.com/subplot/subplot
