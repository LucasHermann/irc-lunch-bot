from datetime import datetime
from time import sleep

from plan import Plan

class Timer:

    def __init__(self, expiry, plans):
        self.expiry = expiry
        self.plans = plans

    def notify(self):
        sleep(10)
        for plan in self.plans:
            plan.notify()

