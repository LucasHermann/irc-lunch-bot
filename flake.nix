{
  description = "Flake definition for my IRC Lunch Bot";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem ( system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
      packageName = "irc-lunch-bot";
    in rec {
      # `nix build`
      packages.${packageName} = pkgs.python39Packages.buildPythonApplication {
        pname = packageName;
        version = "0.0.1";
        src =  ./.;
        propagatedBuildInputs = with pkgs; [
          python39
        ];
        docheck = true;
        checkInputs = [ pkgs.python39Packages.pytest ];
        checkPhase = "pytest tests/";
      };

      defaultPackage = packages.${packageName};

      devShell = pkgs.mkShell {
        BuildInputs = with pkgs; [
          python39Packages.pip
          python39Packages.black
          python39Packages.pytest
        ];
        inputsFrom = builtins.attrValues self.packages.${system};
      };
    });
}

