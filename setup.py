from setuptools import setup, find_packages

setup(
    name='irc_lunch_bot',
    version='0.0.1',
    package_dir = {"":"src"},
    packages=find_packages(
        where = 'src',
        include = ['*'],
    ),
    install_requires=[
    ],
)
