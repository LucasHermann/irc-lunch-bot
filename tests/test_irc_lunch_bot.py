import unittest

from datetime import datetime, timezone
from unittest.mock import patch

import irc_lunch_bot as sut


@patch("irc_lunch_bot.LunchBot.create_timer")
@patch("irc_lunch_bot.Queue")
@patch("irc_lunch_bot.Plan")
class TestPlans(unittest.TestCase):

    def setUp(self):
        self.test_plan_name = "test_plan_name"
        self.test_plan_organizer = "test_plan_organizer"
        self.test_plan_date = datetime(2021, 10, 20, 9, 30, tzinfo=timezone.utc)
        self.test_plan_attendees = set(["test_attendee"])

    def create_plan(self, bot, name="", organizer="", date="", attendees=None):
        if attendees:
            bot.create_plan(name or self.test_plan_name, organizer or self.test_plan_organizer, date or self.test_plan_date, attendees)
        else:
            bot.create_plan(name or self.test_plan_name, organizer or self.test_plan_organizer, date or self.test_plan_date)

    def test_create_no_attendees_no_timer(self, mock_plan, mock_queue, mock_timer_creator):
        expected_date = self.test_plan_date.replace(second=0, microsecond=0)
        test_bot = sut.LunchBot(mock_queue)

        self.create_plan(test_bot, attendees=None)
        result = test_bot.timers[expected_date][0]

        mock_plan.assert_called_once_with(self.test_plan_name, self.test_plan_organizer, expected_date, set())
        mock_timer_creator.assert_called_once_with(expected_date)

        self.assertEqual(result.name, mock_plan().name)
        self.assertEqual(result.organizer, mock_plan().organizer)
        self.assertEqual(result.date, mock_plan().date)

    def test_create_no_attendees_existing_timer(self, mock_plan, mock_queue, mock_timer_creator):
        expected_date = self.test_plan_date.replace(second=0, microsecond=0)
        test_bot = sut.LunchBot(mock_queue)
        test_bot.timers[expected_date] = []

        self.create_plan(test_bot, attendees=None)
        result = test_bot.timers[expected_date][0]

        mock_plan.assert_called_once_with(self.test_plan_name, self.test_plan_organizer, expected_date, set())
        mock_timer_creator.assert_not_called()

        self.assertEqual(result.name, mock_plan().name)
        self.assertEqual(result.organizer, mock_plan().organizer)
        self.assertEqual(result.date, mock_plan().date)

    def test_create_with_attendees_no_timer(self, mock_plan, mock_queue, mock_timer_creator):
        expected_date = self.test_plan_date.replace(second=0, microsecond=0)
        test_bot = sut.LunchBot(mock_queue)

        self.create_plan(test_bot, attendees=self.test_plan_attendees)
        result = test_bot.timers[expected_date][0]

        mock_plan.assert_called_once_with(self.test_plan_name, self.test_plan_organizer, expected_date, self.test_plan_attendees)
        mock_timer_creator.assert_called_once_with(expected_date)

        self.assertEqual(result.name, mock_plan().name)
        self.assertEqual(result.organizer, mock_plan().organizer)
        self.assertEqual(result.date, mock_plan().date)

    def test_create_with_attendees_existing_timer(self, mock_plan, mock_queue, mock_timer_creator):
        expected_date = self.test_plan_date.replace(second=0, microsecond=0)
        test_bot = sut.LunchBot(mock_queue)
        test_bot.timers[expected_date] = []

        self.create_plan(test_bot, attendees=self.test_plan_attendees)
        result = test_bot.timers[expected_date][0]

        mock_plan.assert_called_once_with(self.test_plan_name, self.test_plan_organizer, expected_date, self.test_plan_attendees)
        mock_timer_creator.assert_not_called()

        self.assertEqual(result.name, mock_plan().name)
        self.assertEqual(result.organizer, mock_plan().organizer)
        self.assertEqual(result.date, mock_plan().date)

    def test_delete_existing_plan_existing_timer(self, mock_plan, mock_queue, mock_timer_creator):
        test_bot = sut.LunchBot(mock_queue)
        test_date = "test_date"
        test_plan = mock_plan
        test_plan.date = test_date
        test_bot.timers[test_date] = [test_plan]

        test_bot.delete_plan(test_plan)

        self.assertEqual(len(test_bot.timers[test_date]), 0)

    def test_delete_no_plan_existing_timer(self, mock_plan, mock_queue, mock_timer_creator):
        test_bot = sut.LunchBot(mock_queue)
        test_date = "test_date"
        test_plan = mock_plan
        test_plan.date = test_date
        test_bot.timers[test_date] = ["not a Plan"]

        test_bot.delete_plan(test_plan)

        self.assertEqual(len(test_bot.timers[test_date]), 1)

    def test_delete_existing_plan_wrong_timer(self, mock_plan, mock_queue, mock_timer_creator):
        test_bot = sut.LunchBot(mock_queue)
        test_plan = mock_plan
        test_plan.date = "invalid date"
        test_bot.timers[self.test_plan_date] = [test_plan]

        test_bot.delete_plan(test_plan)

        self.assertEqual(len(test_bot.timers[self.test_plan_date]), 1)

    def test_delete_existing_plan_no_timer(self, mock_plan, mock_queue, mock_timer_creator):
        test_bot = sut.LunchBot(mock_queue)
        test_plan = mock_plan
        test_plan.date = "invalid date"
        test_bot.timers[self.test_plan_date] = [test_plan]

        test_bot.delete_plan(test_plan)

    def test_update_plan_name(self, mock_plan, mock_queue, mock_timer_creator):
        test_bot = sut.LunchBot(mock_queue)
        test_bot.timers[self.test_plan_date] = [mock_plan]
        new_test_plan_name = "new_test_plan_name"
        test_plan_organizer = mock_plan.organizer
        test_plan_date = mock_plan.date

        test_bot.update_plan(mock_plan, name=new_test_plan_name)

        self.assertEqual(test_bot.timers[self.test_plan_date][0].name, new_test_plan_name)
        self.assertEqual(test_bot.timers[self.test_plan_date][0].organizer, test_plan_organizer)
        self.assertEqual(test_bot.timers[self.test_plan_date][0].date, test_plan_date)

    def test_update_plan_organizer(self, mock_plan, mock_queue, mock_timer_creator):
        test_bot = sut.LunchBot(mock_queue)
        test_bot.timers[self.test_plan_date] = [mock_plan]
        test_plan_name = mock_plan.name
        new_test_plan_organizer = "new_test_plan_organizer"
        test_plan_date = mock_plan.date

        test_bot.update_plan(mock_plan, organizer=new_test_plan_organizer)

        self.assertEqual(test_bot.timers[self.test_plan_date][0].name, test_plan_name)
        self.assertEqual(test_bot.timers[self.test_plan_date][0].organizer, new_test_plan_organizer)
        self.assertEqual(test_bot.timers[self.test_plan_date][0].date, test_plan_date)

    def test_update_plan_date(self, mock_plan, mock_queue, mock_timer_creator):
        test_bot = sut.LunchBot(mock_queue)
        test_bot.timers[self.test_plan_date] = [mock_plan]
        test_plan_name = mock_plan.name
        test_plan_organizer = mock_plan.organizer
        new_test_plan_date = datetime(2022, 10, 20, 9, 30, tzinfo=timezone.utc)
        new_expected_test_plan_date = new_test_plan_date.replace(second=0, microsecond=0)

        test_bot.update_plan(mock_plan, date=new_test_plan_date)

        self.assertEqual(test_bot.timers[new_expected_test_plan_date][0].name, test_plan_name)
        self.assertEqual(test_bot.timers[new_expected_test_plan_date][0].organizer, test_plan_organizer)
        self.assertEqual(test_bot.timers[new_expected_test_plan_date][0].date, new_test_plan_date)

    def test_update_plan_name_and_organizer(self, mock_plan, mock_queue, mock_timer_creator):
        test_bot = sut.LunchBot(mock_queue)
        test_bot.timers[self.test_plan_date] = [mock_plan]
        new_test_plan_name = "new_test_plan_name"
        new_test_plan_organizer = "new_test_plan_organizer"
        test_plan_date = mock_plan.date

        test_bot.update_plan(mock_plan, name=new_test_plan_name, organizer=new_test_plan_organizer)

        self.assertEqual(test_bot.timers[self.test_plan_date][0].name, new_test_plan_name)
        self.assertEqual(test_bot.timers[self.test_plan_date][0].organizer, new_test_plan_organizer)
        self.assertEqual(test_bot.timers[self.test_plan_date][0].date, test_plan_date)

    def test_update_plan_name_and_date(self, mock_plan, mock_queue, mock_timer_creator):
        test_bot = sut.LunchBot(mock_queue)
        test_bot.timers[self.test_plan_date] = [mock_plan]
        new_test_plan_name = "new_test_plan_name"
        test_plan_organizer = mock_plan.organizer
        new_test_plan_date = datetime(2022, 10, 20, 9, 30, tzinfo=timezone.utc)
        new_expected_test_plan_date = new_test_plan_date.replace(second=0, microsecond=0)

        test_bot.update_plan(mock_plan, name=new_test_plan_name, date=new_test_plan_date)

        self.assertEqual(test_bot.timers[new_expected_test_plan_date][0].name, new_test_plan_name)
        self.assertEqual(test_bot.timers[new_expected_test_plan_date][0].organizer, test_plan_organizer)
        self.assertEqual(test_bot.timers[new_expected_test_plan_date][0].date, new_test_plan_date)

    def test_update_plan_name_and_organizer_and_date(self, mock_plan, mock_queue, mock_timer_creator):
        test_bot = sut.LunchBot(mock_queue)
        test_bot.timers[self.test_plan_date] = [mock_plan]
        new_test_plan_name = "new_test_plan_name"
        new_test_plan_organizer = "new_test_plan_organizer"
        new_test_plan_date = datetime(2022, 10, 20, 9, 30, tzinfo=timezone.utc)
        new_expected_test_plan_date = new_test_plan_date.replace(second=0, microsecond=0)

        test_bot.update_plan(mock_plan, name=new_test_plan_name, organizer=new_test_plan_organizer, date=new_test_plan_date)

        self.assertEqual(test_bot.timers[new_expected_test_plan_date][0].name, new_test_plan_name)
        self.assertEqual(test_bot.timers[new_expected_test_plan_date][0].organizer, new_test_plan_organizer)
        self.assertEqual(test_bot.timers[new_expected_test_plan_date][0].date, new_test_plan_date)

    def test_update_plan_organizer_and_date(self, mock_plan, mock_queue, mock_timer_creator):
        test_bot = sut.LunchBot(mock_queue)
        test_bot.timers[self.test_plan_date] = [mock_plan]
        test_plan_name = mock_plan.name
        new_test_plan_organizer = "new_test_plan_organizer"
        new_test_plan_date = datetime(2022, 10, 20, 9, 30, tzinfo=timezone.utc)
        new_expected_test_plan_date = new_test_plan_date.replace(second=0, microsecond=0)

        test_bot.update_plan(mock_plan, organizer=new_test_plan_organizer, date=new_test_plan_date)

        self.assertEqual(test_bot.timers[new_expected_test_plan_date][0].name, test_plan_name)
        self.assertEqual(test_bot.timers[new_expected_test_plan_date][0].organizer, new_test_plan_organizer)
        self.assertEqual(test_bot.timers[new_expected_test_plan_date][0].date, new_test_plan_date)

